package be.holdes.aems.webservices.helpers;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogHelper {

    public static void logInfo(String s) {
        getLogger().info(s);
    }

    public static void logWarn(String s) {
        getLogger().warn(s);
    }

    public static void logError(String s) {
        getLogger().error(s);
    }

    public static void logQuery(Query q) {
        logQuery(q, "");
    }

    // Allow to log query param String (added in HQL with setParameter)
    public static void logQuery(Query q, Object... params) {
        //AbstractQueryImpl apq = (AbstractQueryImpl) q;
        String tmp = q.getQueryString();
        for (Object o : params) {
            String oS = o == null ? null : o.toString();
            if (oS == null) {
                oS = "null";
            }
            if (!oS.isEmpty()) {
                tmp += " - " + oS;
            }
        }
        logInfo(tmp);
    }

    public static void logException(Throwable e) {
        getLogger().error(e.toString());
        getLogger().error(e.getMessage());
        logStackTrace(e);
    }

    public static Logger getLogger() {
        return LoggerFactory.getLogger("aems-ws");
    }

    private static void logStackTrace(Throwable e) {
        try (Writer w = new StringWriter(); PrintWriter pw = new PrintWriter(w)) {
            e.printStackTrace(pw);
            getLogger().error(w.toString());
            System.out.println(w.toString());
        } catch (Exception ex) {
            // Do nothing
        }
    }
}
