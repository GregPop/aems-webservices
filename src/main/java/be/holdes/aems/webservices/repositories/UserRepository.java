package be.holdes.aems.webservices.repositories;

import be.holdes.aems.webservices.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    @Query("select u from User u where u.shortName = ?1 and u.status <> 'DELETED'")
    User findOneByShortName(String shortName);

}
