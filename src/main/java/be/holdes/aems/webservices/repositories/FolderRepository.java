package be.holdes.aems.webservices.repositories;

import be.holdes.aems.webservices.model.Folder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface FolderRepository extends JpaRepository<Folder, Integer> {

    @Query("select fo from Folder fo where fo.id = ?1 and fo.isDeleted is false")
    Folder findOneById(Integer id);

}
