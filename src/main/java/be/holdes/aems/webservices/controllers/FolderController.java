package be.holdes.aems.webservices.controllers;

import be.holdes.aems.webservices.helpers.LogHelper;
import be.holdes.aems.webservices.model.Folder;
import be.holdes.aems.webservices.repositories.FolderRepository;
import be.holdes.aems.webservices.responses.FolderOneResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/folder")
public class FolderController {

    @Autowired
    FolderRepository folderRepo;

    @GetMapping("/{id}")
    public ResponseEntity<FolderOneResponse> one(@PathVariable Integer id) throws Exception {
        Folder fo = folderRepo.findOneById(id);
        if (fo == null) {
            LogHelper.logWarn("Folder not found in DB: " + id);
            return ResponseEntity.notFound().build();
        }
        FolderOneResponse res = new FolderOneResponse();
        res.setId(fo.getId());
        LogHelper.logInfo("Folder retrieved: " + id);
        return ResponseEntity.ok().body(res);
    }

}
