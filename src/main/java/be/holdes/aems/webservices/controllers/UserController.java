package be.holdes.aems.webservices.controllers;

import be.holdes.aems.webservices.repositories.UserRepository;
import java.security.Principal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserRepository userRepo;

    @GetMapping("/info")
    //@PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<String> info(Principal principal) throws Exception {
        return ResponseEntity.ok().body(principal.getName());
    }

}
