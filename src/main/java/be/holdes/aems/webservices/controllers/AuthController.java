package be.holdes.aems.webservices.controllers;

import be.holdes.aems.webservices.helpers.LogHelper;
import be.holdes.aems.webservices.model.User;
import be.holdes.aems.webservices.repositories.UserRepository;
import be.holdes.aems.webservices.requests.AuthLoginRequest;
import be.holdes.aems.webservices.responses.AuthLoginResponse;
import be.holdes.aems.webservices.security.JwtTokenUtil;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    UserRepository userRepo;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @PostMapping("/login")
    public ResponseEntity<AuthLoginResponse> login(@Valid @RequestBody AuthLoginRequest req) throws Exception {
        User u = userRepo.findOneByShortName(req.getUsername());
        if (u == null) {
            LogHelper.logWarn("Username not found in DB: " + req.getUsername());
            return ResponseEntity.notFound().build();
        }
        if (!req.getPassword().equals(u.getPassword())) {
            LogHelper.logWarn("Password doesn't match for user: " + req.getUsername());
            return ResponseEntity.notFound().build();
        }
        AuthLoginResponse res = new AuthLoginResponse();
        res.setToken(jwtTokenUtil.generateToken(u.getShortName()));
        LogHelper.logInfo("New token generated for user: " + u.getShortName());
        return ResponseEntity.ok().body(res);
    }

}
