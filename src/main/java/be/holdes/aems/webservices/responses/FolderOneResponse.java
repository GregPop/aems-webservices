package be.holdes.aems.webservices.responses;

public class FolderOneResponse {

    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
