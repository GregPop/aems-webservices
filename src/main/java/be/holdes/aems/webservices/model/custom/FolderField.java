package be.holdes.aems.webservices.model.custom;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class FolderField implements Comparable<FolderField> {

    private Integer id;
    private String name;
    private String displayName;
    private String displayChoices;
    private String type;
    private Integer size;
    private String constraints;
    private String choices;
    private String parentName;
    private Integer moduleId;

    private final List<FolderField> children;

    private String valueVarchar;
    private Integer valueInt;
    private BigDecimal valueDecimal;
    private Date valueDatetime;

    public FolderField(Integer id, String name, String displayName, String displayChoices,
            String type, Integer size, String constraints, String choices, String parentName, Integer moduleId,
            String valueVarchar, Integer valueInt, BigDecimal valueDecimal, Date valueDatetime) {
        this.id = id;
        this.name = name;
        this.displayName = displayName;
        this.displayChoices = displayChoices;
        this.type = type;
        this.size = size;
        this.constraints = constraints;
        this.choices = choices;
        this.parentName = parentName;
        this.moduleId = moduleId;
        this.valueVarchar = valueVarchar;
        this.valueInt = valueInt;
        this.valueDecimal = valueDecimal;
        this.valueDatetime = valueDatetime;
        this.children = new LinkedList<>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayChoices() {
        return displayChoices;
    }

    public void setDisplayChoices(String displayChoices) {
        this.displayChoices = displayChoices;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getConstraints() {
        return constraints;
    }

    public void setConstraintse(String constraints) {
        this.constraints = constraints;
    }

    public String getChoices() {
        return choices;
    }

    public void setChoices(String choices) {
        this.choices = choices;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public Integer getModuleId() {
        return moduleId;
    }

    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }

    public void addChildren(FolderField ff) {
        children.add(ff);
    }

    public List<FolderField> getChildren() {
        return children;
    }

    public String getValueVarchar() {
        return valueVarchar;
    }

    public void setValueVarchar(String valueVarchar) {
        this.valueVarchar = valueVarchar;
    }

    public Integer getValueInt() {
        return valueInt;
    }

    public void setValueInt(Integer valueInt) {
        this.valueInt = valueInt;
    }

    public BigDecimal getValueDecimal() {
        return valueDecimal;
    }

    public void setValueDecimal(BigDecimal valueDecimal) {
        this.valueDecimal = valueDecimal;
    }

    public Date getValueDatetime() {
        return valueDatetime;
    }

    public void setValueDatetime(Date valueDatetime) {
        this.valueDatetime = valueDatetime;
    }

    public Object getValue() {
        return getValueVarchar() != null ? getValueVarchar()
                : getValueInt() != null ? getValueInt()
                        : getValueDecimal() != null ? getValueDecimal()
                                : getValueDatetime();
    }

    @Override
    public int compareTo(FolderField other) {
        if (other == null) {
            return -1;
        }
        return id.compareTo(other.id);
    }

}
