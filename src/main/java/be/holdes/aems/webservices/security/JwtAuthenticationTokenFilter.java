package be.holdes.aems.webservices.security;

import be.holdes.aems.webservices.helpers.LogHelper;
import be.holdes.aems.webservices.model.User;
import be.holdes.aems.webservices.repositories.UserRepository;
import io.jsonwebtoken.ExpiredJwtException;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        LogHelper.logInfo("Path requested: " + request.getServletPath());
        final String requestHeader = request.getHeader("Authorization");
        if (requestHeader != null && requestHeader.startsWith("Bearer ")) {
            String username = null;
            String authToken = requestHeader.substring(7);
            try {
                username = jwtTokenUtil.parseToken(authToken);
            } catch (ExpiredJwtException e) {
                LogHelper.logError("The token is expired and not valid anymore");
            } catch (Exception e) {
                LogHelper.logError("An error occured during getting username from token");
                LogHelper.logException(e);
            }
            if (username != null) {
                User u = userRepo.findOneByShortName(username);
                if (u != null) {
                    JwtUser user = new JwtUser(u);
                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    LogHelper.logInfo("User is authenticated: " + username);
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                } else {
                    LogHelper.logError("Username not found in DB: " + username);
                }
            }
        }
        chain.doFilter(request, response);
    }

}
