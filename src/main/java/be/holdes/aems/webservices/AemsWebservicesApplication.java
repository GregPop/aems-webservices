package be.holdes.aems.webservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AemsWebservicesApplication {

    public static void main(String[] args) {
        SpringApplication.run(AemsWebservicesApplication.class, args);
    }
}
